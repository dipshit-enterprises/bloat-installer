package enterprises.dipshit.bloatinstaller.exception;

import androidx.annotation.Nullable;

public class BadStatusException extends Exception {
    private final int statusCode;

    public BadStatusException(int statusCode) {
        this.statusCode = statusCode;
    }

    public BadStatusException(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nullable
    @Override
    public String getMessage() {
        return "Server returned status code " + getStatusCode() + ": " + super.getMessage();
    }
}
