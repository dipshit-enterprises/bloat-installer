package enterprises.dipshit.bloatinstaller.data;

public class FDroidApp {
    private final String repo;

    private final String id;
    private final String version;
    private final String versionCode;

    // user-friendly shit
    private final String name;
    private final String icon;
    private final String author;
    private final String summary;
    private final String license;
    private final String antiFeatures;

    // package-specific information
    private final String fileName;
    private final String fileSize;
    private final String hash;
    private final String hashType;
    private final String signature;
    private final String permissions;

    public FDroidApp(String repo, String id, String version, String versionCode, String name, String icon, String author, String summary, String license, String antiFeatures, String fileName, String fileSize, String hash, String hashType, String signature, String permissions) {
        this.repo = repo;
        this.id = id;
        this.version = version;
        this.versionCode = versionCode;
        this.name = name;
        this.icon = icon;
        this.author = author;
        this.summary = summary;
        this.license = license;
        this.antiFeatures = antiFeatures;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.hash = hash;
        this.hashType = hashType;
        this.signature = signature;
        this.permissions = permissions;
    }

    public String getRepo() {
        return repo;
    }

    public String getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public String getAuthor() {
        return author;
    }

    public String getSummary() {
        return summary;
    }

    public String getLicense() {
        return license;
    }

    public String[] getAntiFeatures() {
        if (antiFeatures == null) return null;
        return antiFeatures.split(",");
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getHash() {
        return hash;
    }

    public String getHashType() {
        return hashType;
    }

    public String getSignature() {
        return signature;
    }

    public String[] getPermissions() {
        if (antiFeatures == null) return null;
        return permissions.split(",");
    }

    public String getIconUrl() {
        return getIconUrl(repo);
    }

    /**
     * determines the possible URL for the icon for this app
     * there are more than one, and sometimes some lower-priority have icons while higer-priority repos lack thm
     * @param useRepo use a different repo instead of the one this app came from
     * @return a list of possible icon locations
     */
    public String getIconUrl(String useRepo) {
        // icon is either specified in repo index, or located in the default location, or non-existent
        if (icon == null) {
            // hard-coded locale because what the fuck else would I put there
            return useRepo + "/" + id + "/en-US/icon.png";
        }
        return useRepo + "/icons/" + icon;
    }

    public String getDownloadUrl() {
        return repo + "/" + fileName;
    }

    @Override
    public String toString() {
        return "FDroidApp{" +
                "repo='" + repo + '\'' +
                ", id='" + id + '\'' +
                ", version='" + version + '\'' +
                ", versionCode='" + versionCode + '\'' +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", author='" + author + '\'' +
                ", summary='" + summary + '\'' +
                ", license='" + license + '\'' +
                ", antiFeatures='" + antiFeatures + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", hash='" + hash + '\'' +
                ", hashType='" + hashType + '\'' +
                ", signature='" + signature + '\'' +
                ", permissions='" + permissions + '\'' +
                '}';
    }
}
