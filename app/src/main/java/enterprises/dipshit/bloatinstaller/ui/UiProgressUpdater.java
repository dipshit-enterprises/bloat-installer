package enterprises.dipshit.bloatinstaller.ui;

import android.app.Activity;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.function.BiConsumer;

import enterprises.dipshit.bloatinstaller.utils.Units;

public class UiProgressUpdater implements BiConsumer<Long, Long> {
    private boolean connecting;
    private final Activity context;
    private final TextView progressText;
    private final TextView actionText;
    private final ProgressBar progressBar;
    private final String messageDownloading;

    public UiProgressUpdater(Activity context, TextView progressText, TextView actionText, ProgressBar progressBar, String messageDownloading) {
        connecting = true;
        this.context = context;
        this.progressText = progressText;
        this.actionText = actionText;
        this.progressBar = progressBar;
        this.messageDownloading = messageDownloading;
    }

    @Override
    public void accept(Long read, Long total) {
        // todo: do strings correctly

        context.runOnUiThread(() -> {
            // runs once when connection establishes
            if (connecting) {
                connecting = false;
                actionText.setText(messageDownloading);
                progressBar.setIndeterminate(false);
            }

            // do some math (yes, on the ui thread)
            // total can be -1 if content-length is not sent to client
            int percent = total != -1 ? (int) (read * 100 / total) : 1;

            progressBar.setProgress(percent, true);
            progressText.setText("(" + Units.concataByte(read) + " / " + Units.concataByte(total) + ") " + percent + "%");
        });
    }
}
