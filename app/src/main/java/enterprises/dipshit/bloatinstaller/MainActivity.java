package enterprises.dipshit.bloatinstaller;

import static enterprises.dipshit.bloatinstaller.utils.Constants.F_DROID_REPOSITORIES;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.xml.parsers.ParserConfigurationException;

import enterprises.dipshit.bloatinstaller.data.FDroidApp;
import enterprises.dipshit.bloatinstaller.exception.BadStatusException;
import enterprises.dipshit.bloatinstaller.ui.UiProgressUpdater;
import enterprises.dipshit.bloatinstaller.utils.ApiUtil;
import enterprises.dipshit.bloatinstaller.utils.Constants;
import enterprises.dipshit.bloatinstaller.utils.FDroidClient;

@SuppressLint("SetTextI18n") // todo: do strings correctly
public class MainActivity extends AppCompatActivity {
    public static final String INSTALL_FINISHED_INTENT = "enterprises.dipshit.BloatInstaller.AYO_SHIT_INSTALLED";
    private static final String TAG = "MainActivity";
    private LinearLayout appList;
    private ThreadPoolExecutor threadPool;
    private ProgressBar pBar;
    private TextView actionText;
    private TextView progressText;
    private Button retryButton;
    private Button installButton;
    private LinearLayout pageSummary;
    private final Map<Integer, Consumer<Intent>> installCompleteCallbackMap = new HashMap<>();
    private final List<InstallableEntry> installableEntries = new ArrayList<>(Constants.PACKAGES_FALLBACK.length);
    private final Set<String> installedPackages = new HashSet<>(); // an obvious choice

    private static class InstallableEntry {
        CheckBox checkBox;
        Runnable installProcedure;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BroadcastReceiver brrrr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "SHIT INSTALL");
                System.out.println(intent);
                int sessionId = intent.getIntExtra(PackageInstaller.EXTRA_SESSION_ID, 8008135);
                int status = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, 8008135);
                Log.v(TAG, "session id: " + sessionId);

                // there will (probably) be a second intent if pending user action
                Consumer<Intent> callback = status == PackageInstaller.STATUS_PENDING_USER_ACTION ?
                        installCompleteCallbackMap.getOrDefault(sessionId, null) :
                        installCompleteCallbackMap.remove(sessionId);
                if (callback == null) {
                    Log.e(TAG, "Could not find a matching callback!!!");
                    return;
                }

                callback.accept(intent);
            }
        };

        IntentFilter filter = new IntentFilter(INSTALL_FINISHED_INTENT);
        ContextCompat.registerReceiver(this, brrrr, filter, "android.permission.INSTALL_PACKAGES", null, ContextCompat.RECEIVER_NOT_EXPORTED);

        threadPool = new ThreadPoolExecutor(8, 16, 500, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

        appList = findViewById(R.id.appList);
        pBar = findViewById(R.id.syncPBar);
        actionText = findViewById(R.id.actionText);
        progressText = findViewById(R.id.progressText);
        retryButton = findViewById(R.id.retryButton);
        pageSummary = findViewById(R.id.pageSummary);
        installButton = findViewById(R.id.installButton);

        pBar.setMax(100);

        syncRepositories();

        installButton.setOnClickListener(e -> {
            installButton.setEnabled(false);
            for (InstallableEntry entry : installableEntries) {
                entry.checkBox.setEnabled(false);
                if (entry.checkBox.isChecked()) {
                    entry.installProcedure.run();
                }
            }
        });
    }

    public void syncRepositories() {
        Thread thread = new Thread(() -> {
            List<String> ids = new ArrayList<>(Arrays.asList(Constants.PACKAGES_FALLBACK));
            List<FDroidApp> apps = new ArrayList<>(Constants.PACKAGES_FALLBACK.length);

            for (String repo : F_DROID_REPOSITORIES) {
                runOnUiThread(() -> {
                    pBar.setProgress(0, false);
                    pBar.setIndeterminate(true);
                    pBar.setVisibility(View.VISIBLE);
                    actionText.setText("connecting to " + repo);
                    actionText.setVisibility(View.VISIBLE);
                    progressText.setText("please wait");
                    progressText.setVisibility(View.VISIBLE);
                    retryButton.setVisibility(View.GONE);
                });

                Log.i(TAG, "Syncing index for " + repo);
                FDroidClient client = new FDroidClient(repo);

                byte[] indexBin;
                try {
                    UiProgressUpdater progUpdater = new UiProgressUpdater(MainActivity.this, progressText, actionText, pBar, "downloading index for " + repo);
                    indexBin = client.getFDroidIndex(progUpdater);
                } catch (IOException | BadStatusException e) {
                    Log.e(TAG, "Failed to download index", e);
                    runOnUiThread(() -> {
                        actionText.setText("Failed to download repository index for " + repo + ": " + e.getClass().getName() + "\n" + e.getMessage());
                        actionText.setVisibility(View.VISIBLE);
                        progressText.setVisibility(View.GONE);
                        pBar.setVisibility(View.GONE);
                        retryButton.setOnClickListener(event -> syncRepositories());
                        retryButton.setVisibility(View.VISIBLE);
                    });
                    return;
                }

                runOnUiThread(() -> {
                    pBar.setIndeterminate(true);
                    actionText.setText("processing package list...");
                    progressText.setVisibility(View.GONE);
                });
                Document index;
                try {
                    index = client.processFDroidIndex(indexBin);
                } catch (ParserConfigurationException | IOException | SAXException e) {
                    Log.e(TAG, "Failed to parse index", e);
                    runOnUiThread(() -> {
                        actionText.setText("Failed to parse repository index: " + e.getClass().getName() + "\n" + e.getMessage());
                        actionText.setVisibility(View.VISIBLE);
                        progressText.setVisibility(View.GONE);
                        pBar.setVisibility(View.GONE);
                        retryButton.setOnClickListener(event -> syncRepositories());
                        retryButton.setVisibility(View.VISIBLE);
                    });
                    return;
                }


                runOnUiThread(() -> actionText.setText("finding packages..."));
                apps.addAll(client.filterForApplications(index, ids));  // note: this removes ids of apps that were found
            }

            runOnUiThread(() -> actionText.setText("finding already installed packages..."));
            updateInstalledAppList();

            runOnUiThread(() -> actionText.setText("dramatic pause..."));
            try {Thread.sleep(1000);} catch (InterruptedException ignored) {}

            for (FDroidApp app : apps) {
                System.out.println(app);
                runOnUiThread(() -> fuck(app));
            }

            runOnUiThread(() -> {
                progressText.setVisibility(View.GONE);
                actionText.setVisibility(View.GONE);
                pBar.setVisibility(View.GONE);
            });

            Log.i(TAG, "missing " + ids.size());
            for (String id : ids) {
                runOnUiThread(() -> fuck(new FDroidApp(
                        "not-real",
                        id,
                        "unknown",
                        "-1",
                        id + " (not found)",
                        null,
                        null,
                        "App was not found in repository",
                        null,
                        null,
                        "/dev/null",
                        "0",
                        "deadbeef",
                        "sha-256",
                        "deadbeef",
                        null
                )));
            }

            //about repo:
            //  repo
            //    [pubkey]

            System.out.println("done");

            runOnUiThread(() -> pageSummary.setVisibility(View.VISIBLE));
        });
        thread.start();
    }

    public void updateInstalledAppList() {
        PackageManager packageManager = getApplicationContext().getPackageManager();
        installedPackages.clear();
        for (ApplicationInfo appInfo : packageManager.getInstalledApplications(0)) {
            installedPackages.add(appInfo.packageName);
        }
    }

    public void installPackage(String packageName, InputStream is, Consumer<Intent> doOnComplete) throws IOException {
        PackageInstaller packageInstaller = getApplicationContext().getPackageManager().getPackageInstaller();
        PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
        params.setAppPackageName(packageName);

        int sessionId = packageInstaller.createSession(params);
        Intent explicitFuckingIntent = new Intent(INSTALL_FINISHED_INTENT);
        explicitFuckingIntent.setPackage(getPackageName());
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(
                        getApplicationContext(),
                        sessionId,
                        explicitFuckingIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE);

        try (PackageInstaller.Session session = packageInstaller.openSession(sessionId)) {
            try (OutputStream out = session.openWrite("dipshit", 0, -1)) {
                byte[] buffer = new byte[4096];
                int bytes;
                while ((bytes = is.read(buffer)) != -1) out.write(buffer, 0, bytes);

                session.fsync(out);
            }
            installCompleteCallbackMap.put(sessionId, doOnComplete);
            session.commit(pendingIntent.getIntentSender());
        }
    }

    public void loadIconAsync(FDroidApp app, ImageView imageView) {
        loadIconAsync(app, imageView, null, 0);
    }

    public void loadIconAsync(FDroidApp app, ImageView imageView, String repo, int tryNumber) {
        if (tryNumber > 4) return; // give up

        threadPool.execute(() -> {
            try {
                String url = repo == null ? app.getIconUrl() : app.getIconUrl(repo);
                byte[] icon = ApiUtil.get(new URL(url));
                Bitmap bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
                runOnUiThread(() -> imageView.setImageBitmap(bitmap));
                return;
            } catch (IOException e) {
                Log.w(TAG, "failed to fetch icon :(", e);
                // retrying may fix this
                try {Thread.sleep(500);} catch (InterruptedException ignored) {}
                loadIconAsync(app, imageView, repo, tryNumber + 1);
            } catch (BadStatusException e) {
                Log.w(TAG, "failed to fetch icon :(    [status " + e.getStatusCode() + "]", e);
                // retrying is likely not going to change this server's mind, try another
                // this may seem stupid, but certain apps on the fdroid repo lack icons while izzy's repo does not
                if (repo != null) return; // no fork bomb lmao
                for (String otherRepo : F_DROID_REPOSITORIES) {
                    loadIconAsync(app, imageView, otherRepo, 2);
                }
            }
            // if a repo was defined, this is not the only branch
            if (repo == null) {
                // dipshit icon to indicate failure
                runOnUiThread(() -> imageView.setImageIcon(Icon.createWithResource(appList.getContext(), R.drawable.baseline_android_24).setTint(getResources().getColor(R.color.dipshit, getTheme()))));
            }

        });
    }

    public AlertDialog createAppInfoMenu(FDroidApp app, Drawable preloadedIcon) {
        {
            View menuView = LayoutInflater.from(appList.getContext()).inflate(R.layout.app_info_menu, appList, false);

            ((TextView) menuView.findViewById(R.id.appInfoName)).setText(app.getName());
            ((TextView) menuView.findViewById(R.id.appInfoId)).setText(app.getId());
            ((TextView) menuView.findViewById(R.id.appInfoVersion)).setText(app.getVersion() + " (" + app.getVersionCode() + ")");
            ((ImageView) menuView.findViewById(R.id.appInfoIcon)).setImageDrawable(preloadedIcon);
            LinearLayout attributeListView = menuView.findViewById(R.id.appInfoAttributes);

            String[][] attributeMap = new String[][] {
                    {"summary", app.getSummary()},
                    {"author", app.getAuthor()},
                    {"license", app.getLicense()},
                    {"anti-features", app.getAntiFeatures() != null ? String.join("\n", app.getAntiFeatures()) : null},
                    {"permissions", app.getPermissions() != null ? String.join("\n", app.getPermissions()) : null},
                    {"file name", app.getFileName()},
                    {"file size", app.getFileSize()},
                    {"hash", app.getHash() + "    " + app.getHashType()},   // hash _should_ be always defined
                    {"signature", app.getSignature()},
                    {"F-Droid repository", app.getRepo()}
            };

            for (String[] attribute : attributeMap) {
                if (attribute[1] == null) continue;
                View attributeView = LayoutInflater.from(appList.getContext()).inflate(R.layout.app_info_attribute, attributeListView, false);
                ((TextView) attributeView.findViewById(R.id.attributeTitle)).setText(attribute[0]);
                ((TextView) attributeView.findViewById(R.id.attributeContent)).setText(attribute[1]);
                attributeListView.addView(attributeView);
            }

            return new AlertDialog.Builder(this)
                    .setView(menuView)
                    .setPositiveButton("cool", null)
                    .create();

        }
    }

    public void fuck(FDroidApp app) {
        View entryLayout = LayoutInflater.from(appList.getContext()).inflate(R.layout.app_entry, appList, false);

        CheckBox checkBox = entryLayout.findViewById(R.id.checkBox);
        ImageView imageView = entryLayout.findViewById(R.id.appIcon);
        TextView title = entryLayout.findViewById(R.id.appName);
        TextView shortDesc = entryLayout.findViewById(R.id.appSummary);
        TextView installedMarker = entryLayout.findViewById(R.id.installedMarker);
        ImageButton aboutButton = entryLayout.findViewById(R.id.appInfoButton);

        title.setText(app.getName());
        shortDesc.setText(app.getSummary());

        imageView.setImageIcon(Icon.createWithResource(appList.getContext(), R.drawable.baseline_android_24).setTint(getResources().getColor(R.color.white, getTheme())));
        loadIconAsync(app, imageView);

        aboutButton.setOnClickListener(e -> createAppInfoMenu(app, imageView.getDrawable()).show());

        ImageButton retryButton = entryLayout.findViewById(R.id.retryAppInstallButton);
        ProgressBar progressBar = entryLayout.findViewById(R.id.appInstallPBar);
        progressBar.setMax(100);
        TextView actionText = entryLayout.findViewById(R.id.appInstallActionText);
        TextView progressText = entryLayout.findViewById(R.id.appInstallProgressText);

        if (installedPackages.contains(app.getId())) {
            checkBox.setEnabled(false);
            checkBox.setVisibility(View.INVISIBLE);
            installedMarker.setVisibility(View.VISIBLE);
        } else {
            checkBox.setChecked(true);
        }

        appList.addView(entryLayout);

        InstallableEntry entry = new InstallableEntry();
        entry.checkBox = checkBox;
        entry.installProcedure = () -> {
            runOnUiThread(() -> {
                actionText.setText("queued");
                actionText.setVisibility(View.VISIBLE);
            });

            threadPool.execute(() -> {
                runOnUiThread(() -> {
                    progressBar.setProgress(0, false);
                    progressBar.setIndeterminate(true);
                    progressBar.setVisibility(View.VISIBLE);
                    actionText.setText("starting download for " + app.getId());
                    actionText.setVisibility(View.VISIBLE);
                    progressText.setText("connecting");
                    progressText.setVisibility(View.VISIBLE);
                });

                File downloadLocation = getCacheDir().toPath().resolve(UUID.randomUUID().toString()).toFile();
                try {
                    UiProgressUpdater progUpdater = new UiProgressUpdater(MainActivity.this, progressText, actionText, progressBar, "downloading " + app.getId());
                    ApiUtil.get(new URL(app.getDownloadUrl()), progUpdater, downloadLocation);
                } catch (IOException | BadStatusException e) {
                    Log.e(TAG, "Failed to download package", e);
                    runOnUiThread(() -> {
                        actionText.setText("Failed to download app: " + e.getClass().getName() + "\n" + e.getMessage());
                        actionText.setVisibility(View.VISIBLE);
                        progressText.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        retryButton.setVisibility(View.VISIBLE);
                        retryButton.setOnClickListener(ev -> {
                            retryButton.setVisibility(View.GONE);
                            entry.installProcedure.run();
                        });
                    });
                    return;
                }

                runOnUiThread(() -> {
                    progressBar.setIndeterminate(true);
                    progressText.setVisibility(View.GONE);
                    actionText.setText("Installing " + app.getId());
                });

                try (InputStream is = Files.newInputStream(downloadLocation.toPath())) {

                    installPackage(app.getId(), is, intent -> {

                        int status = intent.getIntExtra(PackageInstaller.EXTRA_STATUS, 8008135); // * magic number *
                        String session = intent.getStringExtra(PackageInstaller.EXTRA_SESSION);
                        int sessionId = intent.getIntExtra(PackageInstaller.EXTRA_SESSION_ID, 8008135);

                        Log.v(TAG, "status: " + status);
                        Log.v(TAG, "session: " + session);
                        Log.v(TAG, "session id: " + sessionId);

                        switch (status) {
                            case PackageInstaller.STATUS_SUCCESS:
                                Log.i(TAG, "Install succeeded for " + app.getId());
                                runOnUiThread(() -> {
                                    progressBar.setVisibility(View.GONE);
                                    actionText.setVisibility(View.GONE);
                                    progressText.setVisibility(View.GONE);
                                    installedMarker.setVisibility(View.VISIBLE);
                                    checkBox.setVisibility(View.INVISIBLE);
                                    checkBox.setChecked(false);
                                    checkBox.setEnabled(false);
                                });
                                // it feels stupid wrapping this in try/catch, but I'd rather the app not crash in unforseen scenarios
                                try {
                                    if (!downloadLocation.delete()) Log.w(TAG, "Failed to delete cached apk file " + downloadLocation);
                                } catch (SecurityException e) {
                                    Log.wtf(TAG, "android doesn't want me to delete my temp file at " + downloadLocation, e);
                                }
                                break;
                            case PackageInstaller.STATUS_PENDING_USER_ACTION:
                                Log.i(TAG, "waiting on your stupid ass");
                                Intent confirm = (Intent) Objects.requireNonNull(intent.getExtras()).get(Intent.EXTRA_INTENT);
                                runOnUiThread(() -> startActivity(confirm));
                                break;
                            default:
                                Log.i(TAG, "Install failed probably! " + app.getId());
                                runOnUiThread(() -> {
                                    actionText.setText("Install failed");
                                    actionText.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    progressText.setVisibility(View.GONE);
                                    retryButton.setVisibility(View.VISIBLE);
                                    retryButton.setOnClickListener(ev -> {
                                        retryButton.setVisibility(View.GONE);
                                        entry.installProcedure.run();
                                    });
                                });
                                try {
                                    if (!downloadLocation.delete()) Log.w(TAG, "Failed to delete cached apk file " + downloadLocation);
                                } catch (SecurityException e) {
                                    Log.wtf(TAG, "android doesn't want me to delete my temp file at " + downloadLocation, e);
                                }
                                break;
                        }

                        String statusMessage = intent.getStringExtra(PackageInstaller.EXTRA_STATUS_MESSAGE);
                        if (statusMessage != null) {
                            Log.i(TAG, statusMessage);
                            runOnUiThread(() -> {
                                actionText.setText(statusMessage);
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "Failed to install", e);
                    runOnUiThread(() -> {
                        actionText.setText("Failed to install package: " + e.getClass().getName() + "\n" + e.getMessage());
                        actionText.setVisibility(View.VISIBLE);
                        progressText.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        retryButton.setVisibility(View.VISIBLE);
                        retryButton.setOnClickListener(ev -> {
                            retryButton.setVisibility(View.GONE);
                            entry.installProcedure.run();
                        });
                    });
                }
            });
        };


        installableEntries.add(entry);

    }

}