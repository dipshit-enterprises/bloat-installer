package enterprises.dipshit.bloatinstaller.utils;

public class Constants {
    public static final String[] F_DROID_REPOSITORIES = {
            "https://f-droid.org/repo",
            "https://apt.izzysoft.de/fdroid/repo",
            "https://app.futo.org/fdroid/repo"
    };
    public static final String[] PACKAGES_FALLBACK = {
            // on f-droid
            "org.fitchfamily.android.dejavu",
            "helium314.localbackend",
            "io.github.muntashirakon.AppManager",
            "org.fitchfamily.android.gsmlocation",
            "org.fitchfamily.android.wifi_backend",
            "at.bitfire.davdroid",
            "com.machiav3lli.backup",
            "com.reecedunn.espeak",
            "org.mozilla.fennec_fdroid",
            "us.spotco.fennec_dos",
            "de.marmaro.krt.ffupdater",
            "de.kaffeemitkoffein.imagepipe",
            "com.fsck.k9",
            "com.kunzisoft.keepass.libre",
            "com.nextcloud.client",
            "it.niedermann.owncloud.notes",
            "com.polar.nextcloudservices",
            "org.unifiedpush.distributor.nextpush",
            "net.osmand.plus",
            "us.spotco.maps",
            "com.bobek.compass",
            "im.vector.app",
            "com.nextcloud.talk2",
            "su.xash.husky",
            "org.linphone",
            "org.localsend.localsend_app",
            "com.termux",
            "com.termux.api",
            "com.termux.boot",
            "com.termux.widget",
            "io.github.zyrouge.symphony",
            "com.github.vauvenal5.yaga",
            "it.niedermann.nextcloud.deck",
            "net.eneiluj.nextcloud.phonetrack",
            "com.odysee.floss",
            "org.schabi.newpipe",
            // on izzy
            "com.topjohnwu.magisk",
            "org.polymorphicshade.tubular",
            "org.breezyweather",
            "org.moxxy.moxxyv2",
            "com.wireguard.android",
            "moe.shizuku.privileged.api",
            // on futo
            "org.futo.voiceinput",
            "com.futo.platformplayer"
    };
}
