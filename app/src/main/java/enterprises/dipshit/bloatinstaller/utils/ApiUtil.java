package enterprises.dipshit.bloatinstaller.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.net.ssl.HttpsURLConnection;

import enterprises.dipshit.bloatinstaller.exception.BadStatusException;

public class ApiUtil {
    private static final String TAG = "ApiUtil";

    public static final int CHUNK_SIZE = 16384;

    public static byte[] get(URL endpoint, BiConsumer<Long, Long> progressCallback) throws IOException, BadStatusException {
        return get(endpoint, progressCallback, null);
    }

    public static byte[] get(URL endpoint, BiConsumer<Long, Long> progressCallback, File outputFile) throws IOException, BadStatusException {
        // get content length ahead of time so we can use gzip
        long contentLen = -1;
        try {
            List<String> clHeader = head(endpoint).get("Content-Length");
            if (clHeader == null) throw new Exception("did not find content length in headers");
            String clValue = clHeader.get(0);
            contentLen = Long.parseLong(clValue);

            Log.v(TAG, "content length found! " + contentLen);

            // update progress callback to make it clear a connection established
            progressCallback.accept(0L, contentLen);

        } catch (Exception e) {
            Log.e(TAG, "Exception while getting content length", e);
        }

        HttpsURLConnection connection = (HttpsURLConnection) endpoint.openConnection();

        // set sane settings
        connection.setInstanceFollowRedirects(true);
        connection.setUseCaches(false);
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(7000);
        // gzip actually makes the file-size bearable. only do this if necessary
        if (contentLen == -1) {
            Log.w(TAG, "Not using gzip because of missing content length header");
            connection.setRequestProperty("Accept-Encoding", "identity");   // disable gzip so we can get content-length
        }

        Log.v(TAG, "GET " + endpoint);
        connection.connect();

        int status = connection.getResponseCode();
        if (contentLen == -1) contentLen = connection.getContentLengthLong();

        // handle error
        if (status >= 400) {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()))) {
                StringBuilder errorBuilder = new StringBuilder();
                bufferedReader.lines().forEach(errorBuilder::append);
                throw new BadStatusException(status, errorBuilder.toString());
            }
        }




        try (BufferedInputStream is = new BufferedInputStream(connection.getInputStream());
             OutputStream os = outputFile != null ? Files.newOutputStream(outputFile.toPath()) : new ByteArrayOutputStream()  // output to file instead if specified
             ) {
            // get response in chunks, allowing progress bar to update
            long totalRead = 0;
            byte[] buffer = new byte[CHUNK_SIZE];
            int read;
            while ((read = is.read(buffer)) != -1) {
                os.write(buffer, 0, read);
                totalRead += read;
                progressCallback.accept(totalRead, contentLen);
            }

            // we done
            progressCallback.accept(totalRead, totalRead);
            connection.disconnect();
            if (outputFile == null) return ((ByteArrayOutputStream) os).toByteArray();
        }
        return null;
    }

    /**
     * gets headers and no content. useful for getting content-length header for gzipped endpoints
     */
    public static Map<String, List<String>> head(URL endpoint) throws IOException {
        HttpsURLConnection connection = (HttpsURLConnection) endpoint.openConnection();

        // set sane settings
        connection.setInstanceFollowRedirects(true);
        connection.setUseCaches(false);
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(7000);
        connection.setRequestProperty("Accept-Encoding", "identity");   // disable gzip so we can get content-length
        connection.setRequestMethod("HEAD");

        Log.v(TAG, "HEAD " + endpoint);
        connection.connect();
        Map<String, List<String>> headers = connection.getHeaderFields();
        connection.disconnect();
        return headers;
    }

    /**
     * get the bytes located at this URL with no fancy shit like progress and content-length
     */
    public static byte[] get(URL endpoint) throws IOException, BadStatusException {
        HttpsURLConnection connection = (HttpsURLConnection) endpoint.openConnection();

        // set sane settings
        connection.setInstanceFollowRedirects(true);
        connection.setUseCaches(true);
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(7000);

        Log.v(TAG, "GET " + endpoint);
        connection.connect();

        int status = connection.getResponseCode();

        // (don't) handle error
        if (status >= 400) {
            throw new BadStatusException(status);
        }

        // just get the response.
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        BufferedInputStream reader = new BufferedInputStream(connection.getInputStream());

        byte[] buffer = new byte[CHUNK_SIZE]; // the buffer for the buffer, yes I know it's stupid
        int shit;
        while ((shit = reader.read(buffer)) != -1) {
            os.write(buffer, 0, shit);
        }

        // we done
        connection.disconnect();
        return os.toByteArray();
    }

}
