package enterprises.dipshit.bloatinstaller.utils;

import android.util.Log;

public class Units {

    private static final String[] byteUnitNames = {"B", "KB", "MB", "GB", "TB", "PB"};

    /**
     * concatenates byte amounts by factors of 10^3 up to petabyte for human-readable strings
     * @param byteAmount number of bytes
     * @return a human-readable string instead of just a giant fuck-off number
     */
    public static String concataByte(long byteAmount) {
        long num = byteAmount;

        for (int i = 0; i < byteUnitNames.length; i++) {
            if (num < 1000) {
                long bigNum = (long) (byteAmount / Math.pow(10, i * 3 - 2));
                return (bigNum / 100) + "." + (bigNum % 100) + " " + byteUnitNames[i];
            }
            num /= 1000;
        }
        Log.wtf("concataByte", "really large fucking number");
        return num + " " + byteUnitNames[byteUnitNames.length - 1];
    }
}
