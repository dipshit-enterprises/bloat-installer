package enterprises.dipshit.bloatinstaller.utils;

import android.os.Build;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import enterprises.dipshit.bloatinstaller.data.FDroidApp;
import enterprises.dipshit.bloatinstaller.exception.BadStatusException;

public class FDroidClient {
    private static final String TAG = "FDroidClient";

    private final String repo;

    public FDroidClient(String repo) {
        this.repo = repo;
    }

    public byte[] getFDroidIndex(BiConsumer<Long, Long> progressCallback) throws BadStatusException, IOException {
        URL fDroidRepo;
        try {
            fDroidRepo = new URL(repo + "/index.xml");
        } catch (MalformedURLException e) {
            Log.wtf(TAG, "the hard-coded url is allegedly malformed");
            throw new RuntimeException(e);
        }

        Log.v(TAG, "Downloading repository index");
        return ApiUtil.get(fDroidRepo, progressCallback);
    }

    public Document processFDroidIndex(byte[] index) throws ParserConfigurationException, IOException, SAXException {
        Log.v(TAG, "Parsing result");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(new ByteArrayInputStream(index));
    }

    public List<FDroidApp> filterForApplications(Document document, List<String> packageIds) {
        // note: does modify the contents of packageIds
        List<FDroidApp> matches = new ArrayList<>(packageIds.size());
        NodeList applications = document.getElementsByTagName("application");

        for (int i = 0; i < applications.getLength(); i++) {
            Node node = applications.item(i);
            String id = node.getAttributes().getNamedItem("id").getTextContent();

            if (packageIds.contains(id)) {
                matches.add(toApp(node));
                packageIds.remove(id);

                if (packageIds.size() == 0) break;
            }
        }
        return matches;
    }

    public FDroidApp toApp(Node app) {

        // get information about the app
        String id = app.getAttributes().getNamedItem("id").getTextContent();
        String version = null;
        String versionCode = null;
        String summary = null;
        String name = null;
        String icon = null;
        String author = null;
        String license = null;
        String antiFeatures = null;

        Map<Long, Node> supportedPackages = new HashMap<>();
        NodeList attrs = app.getChildNodes();
        for (int i = 0; i < attrs.getLength(); i++) {
            Node attr = attrs.item(i);
            switch (attr.getNodeName()) {
                case "marketvercode": versionCode = attr.getTextContent(); break;
                case "marketversion": version = attr.getTextContent(); break;
                case "summary": summary = attr.getTextContent(); break;
                case "name": name = attr.getTextContent(); break;
                case "icon": icon = attr.getTextContent(); break;
                case "author": author = attr.getTextContent(); break;
                case "license": license = attr.getTextContent(); break;
                case "antifeatures": antiFeatures = attr.getTextContent(); break;
                case "package": {
                    List<String> supportedArch = null;
                    Long packageVersionCode = null;

                    // check compatibility
                    NodeList pAttrs = attr.getChildNodes();
                    for (int j = 0; j < pAttrs.getLength(); j++) { // "nativecode" is usually at the end
                        Node pAttr = pAttrs.item(j);

                        if (pAttr.getNodeName().equals("versioncode")) {
                            try {
                                packageVersionCode = Long.parseLong(pAttr.getTextContent());
                            } catch (NumberFormatException e) {
                                Log.wtf(TAG, "version code is not a fucking number?", e);
                            }
                        } else if (pAttr.getNodeName().equals("nativecode")) {
                            supportedArch = Arrays.asList(pAttr.getTextContent().split(","));
                        }

                        if (supportedArch != null && packageVersionCode != null) break;
                    }

                    // apk is supported if it shares a common architecture or doesn't require one
                    boolean supported = false;
                    if (supportedArch != null) {
//                        Arrays.stream(Build.SUPPORTED_ABIS).filter(s -> supportedArch.contains(s)).findAny().isPresent()
                        for (String arch : Build.SUPPORTED_ABIS) {
                            if (!supportedArch.contains(arch)) continue;
                            supported = true;
                            break;
                        }
                    } else supported = true;

                    if (supported) {
                        if (packageVersionCode == null) {
                            Log.wtf(TAG, "package entry does not contain a version code! what in the actual fuck!");
                            packageVersionCode = -1L; // idk man
                        }
                        supportedPackages.put(packageVersionCode, attr);
                    }

                    break;
                }
            }
        }

        Node recommendedPackage = null;
        Long[] sortedPackageVersions = supportedPackages.keySet().toArray(new Long[0]);
        Arrays.sort(sortedPackageVersions, Comparator.reverseOrder());

        if (sortedPackageVersions.length == 0) Log.e(TAG, "no supported packages found for " + id);
        else if (versionCode == null) recommendedPackage = supportedPackages.get(sortedPackageVersions[0]);
        else {
            long mvc;
            try {
                mvc = Long.parseLong(versionCode);
                recommendedPackage = supportedPackages.getOrDefault(mvc, null);

                if (recommendedPackage == null) {
                    // find the next highest supported version
                    for (long vc : sortedPackageVersions) {
                        if (vc > mvc) continue;
                        recommendedPackage = supportedPackages.get(mvc);
                        break;
                    }
                }

                if (recommendedPackage == null) {
                    Log.w(TAG, "exhausted all sensible ways of picking an application version. just going with the last one in the list");
                    recommendedPackage = supportedPackages.get(sortedPackageVersions[sortedPackageVersions.length - 1]);
                }
            } catch (NumberFormatException e) {
                Log.wtf(TAG, "market version code was not numeric! this is problematic!!!");
                recommendedPackage = supportedPackages.get(sortedPackageVersions[0]);
            }

        }


        // get information about the recommended package
        String fileName = null;
        String filesize = null;
        String hash = null;
        String hashtype = null;
        String sig = null;
        String permissions = null;

        if (recommendedPackage != null) {
            attrs = recommendedPackage.getChildNodes();
            for (int i = 0; i < attrs.getLength(); i++) {
                Node attr = attrs.item(i);
                switch (attr.getNodeName()) {
                    case "apkname": fileName = attr.getTextContent(); break;
                    case "size": filesize = attr.getTextContent(); break;
                    case "hash": hash = attr.getTextContent(); break;
                    //case "hash-type": hashtype = attr.getTextContent(); break; <- attribute of hash
                    case "sig": sig = attr.getTextContent(); break;
                    case "permissions": permissions = attr.getTextContent(); break;
                }
            }
        }

        return new FDroidApp(
                repo,
                id,
                version,
                versionCode,
                name,
                icon,
                author,
                summary,
                license,
                antiFeatures,
                fileName,
                filesize,
                hash,
                hashtype,
                sig,
                permissions
        );

    }
}
